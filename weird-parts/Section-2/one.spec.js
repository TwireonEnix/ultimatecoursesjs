describe('The execution context', () => {
  it('first example: hoisting', () => {
    b();
    console.log(a);
    var a = 'Hello world';
    function b() {
      console.log('B Called!');
    }
  });

  it('second example: the undefined value', () => {
    const defined = a =>
      a === undefined ? console.log('a is undefined') : console.log('a is defined');
    const a = undefined;
    console.log(a);
    defined(a);
    // a = null;
    // defined(a);
  });

  it('third example: function invocation', () => {
    const b = () => {
      console.log('called b');
      var bv;
    };
    const a = () => {
      b();
      console.log('called a');
    };
    a();
  });

  it('fourth example: variable environments', () => {
    const b = () => {
      console.log(thisVar);
    };
    const a = () => {
      var thisVar = 2;
      console.log(thisVar);
      b();
    };
    const thisVar = 1;
    console.log(thisVar);
    a();
    console.log(thisVar);
  });
});
